## Lily Pads

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/impression-back-island.PNG)

The map requested by a community member of Forged Alliance Forever (FAF), designed and made by myself. The map is designed in such a way that the typical 'I don't know who to strike' problem with free for all matches dissapear: the player setup is in such a way that one team spawns one player on the smaller islands and one player on two of the three main islands.

The result is that two of the three players of each team have a clear target: the fella on the other side of the main islands. The last player of each team can assist where neccesary.

## Statistics of the map

![](/images/impression-main-island.PNG)

The map has about 61.675 reclaimable mass on it. On each island there are numerous trees and rocks that can be reclaimed.

All players start with 10 - 11 mass extractors around them. On top of that, there is:
 - Various number of trees and rocks to reclaim.
 - Some tanks (t1 / t2) reclaim in the center of each of the main islands.
 - Some boats (t1 / t2) reclaim on one of the side islands and on each of the main islands. 

## Notes about the imagery

![](/images/impression-overall.PNG)

 - The units with orange icons represent the neutral civilians.
 - The units with nearly black icons represent wreckages.

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
