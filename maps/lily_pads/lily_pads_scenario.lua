version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Lily Pads",
    description = "A map on request by LegendSmith, made by Jip. \r\n\r\nFor more information see: \r\nhttps://gitlab.com/w.b.wijnia/lily-pads",
    preview = '',
    map_version = 9,
    type = 'skirmish',
    starts = true,
    size = {1024, 1024},
    reclaim = {66482, 131802.2},
    map = '/maps/lily_pads.v0009/lily_pads.scmap',
    save = '/maps/lily_pads.v0009/lily_pads_save.lua',
    script = '/maps/lily_pads.v0009/lily_pads_script.lua',
    norushradius = 90,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4', 'ARMY_5', 'ARMY_6', 'ARMY_7', 'ARMY_8', 'ARMY_9'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_10 NEUTRAL_CIVILIAN Wreckages' ),
            },
        },
    },
}
